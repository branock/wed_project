<?php
/*
 ./app/controleurs/categoriesControleur.php
*/
namespace App\Controleurs\CategoriesControleur;

function indexAction(\PDO $connexion) {
 // Je mets la liste des categories dans $categories
   include_once '../app/modeles/categoriesModele.php';
   $categories = \App\Modeles\CategoriesModele\findAll($connexion);
 // Je charge la vue index dans $content
   GLOBAL $content;
   ob_start();
     include '../app/vues/categories/index.php';
   $content = ob_get_clean();
}

// Affiche le formulaire d'ajout d'une catégorie
function addFormAction() {
  GLOBAL $content;
  ob_start();
    include '../app/vues/categories/addForm.php';
  $content = ob_get_clean();
}

//ajoute une catégorie suite au formulaire
function addAction(\PDO $connexion) {
  //je demande au modèle d'ajouter la catégorie
  include_once '../app/modeles/categoriesModele.php';
  $id = \App\Modeles\CategoriesModele\insert($connexion);
  //je redirige vers la liste des catégories
  header('location:'. BASE_URL_ADMIN . 'categories');
}

//supprime une catégorie au click
function deleteAction(\PDO $connexion, int $id) {
  //je demande au modèle de supprimer la catégorie
  include_once '../app/modeles/categoriesModele.php';
  $return = \App\Modeles\CategoriesModele\delete($connexion, $id);
  //je redirige vers la liste des catégories
  header('location:'. BASE_URL_ADMIN . 'categories');
}

//Affiche le formulaire de modification de catégorie
function editFormAction(\PDO $connexion, int $id) {
  //je demande au modèle de trouver l'élément
  include_once '../app/modeles/categoriesModele.php';
  $categorie = \App\Modeles\CategoriesModele\findOneById($connexion, $id);
  //je charge la vue editForm dans $content1
  GLOBAL $content;
  ob_start();
    include '../app/vues/categories/editForm.php';
  $content = ob_get_clean();
}

//Modifie une catégorie
function editAction(\PDO $connexion, int $id) {
  //je demande au modèle d'updater' la catégorie
  include_once '../app/modeles/categoriesModele.php';
  $return = \App\Modeles\CategoriesModele\update($connexion, $id);
  //je redirige vers la liste des catégories
  header('location:'. BASE_URL_ADMIN . 'categories');


}
