<?php
/*
 ./app/controleurs/authorsControleur.php
*/
namespace App\Controleurs\AuthorsControleur;

function indexAction(\PDO $connexion) {
 // Je mets la liste des authors dans $authors
   include_once '../app/modeles/authorsModele.php';
   $authors = \App\Modeles\AuthorsModele\findAll($connexion);
 // Je charge la vue index dans $content
   GLOBAL $content;
   ob_start();
     include '../app/vues/authors/index.php';
   $content = ob_get_clean();
}

// Affiche le formulaire d'ajout d'une author
function addFormAction() {
  GLOBAL $content;
  ob_start();
    include '../app/vues/authors/addForm.php';
  $content = ob_get_clean();
}

//ajoute une author suite au formulaire
function addAction(\PDO $connexion) {
  //je demande au modèle d'ajouter la author
  include_once '../app/modeles/authorsModele.php';
  $id = \App\Modeles\authorsModele\insert($connexion);
  //je redirige vers la liste des authors
  header('location:'. BASE_URL_ADMIN . 'authors');
}

//supprime un author au click
function deleteAction(\PDO $connexion, int $id) {
  //je demande au modèle de supprimer l author
  include_once '../app/modeles/authorsModele.php';
  $return = \App\Modeles\authorsModele\delete($connexion, $id);
  //je redirige vers la liste des authors
  header('location:'. BASE_URL_ADMIN . 'authors');
}

//Affiche le formulaire de modification de author
function editFormAction(\PDO $connexion, int $id) {
  //je demande au modèle de trouver l'élément
  include_once '../app/modeles/authorsModele.php';
  $author = \App\Modeles\authorsModele\findOneById($connexion, $id);
  //je charge la vue editForm dans $content
  GLOBAL $content;
  ob_start();
    include '../app/vues/authors/editForm.php';
  $content = ob_get_clean();
}

//Modifie un author
function editAction(\PDO $connexion, int $id) {
  //je demande au modèle d'updater' la author
  include_once '../app/modeles/authorsModele.php';
  $return = \App\Modeles\authorsModele\update($connexion, $id);
  //je redirige vers la liste des authors
  header('location:'. BASE_URL_ADMIN . 'authors');


}
