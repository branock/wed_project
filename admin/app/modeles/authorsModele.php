<?php
/*
 ./app/modeles/authorsModele.php
*/
namespace App\Modeles\authorsModele;

function findAll(\PDO $connexion) {
 $sql = "SELECT *
         FROM authors
         ORDER BY firstname ASC;";
 $rs = $connexion->query($sql);
 return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findOneById(\PDO $connexion, int $id) {
  $sql = "SELECT *
          FROM authors
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}


function insert(\PDO $connexion) {
  $sql = "INSERT INTO authors
          SET firstname = :firstname,
              lastname = :lastname,
              biography = :biography,
              avatar = :avatar,
              created_at = NOW();";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':firstname', $_POST['firstname'], \PDO::PARAM_STR );
  $rs->bindValue(':lastname', $_POST['lastname'], \PDO::PARAM_STR );
  $rs->bindValue(':biography', $_POST['biography'], \PDO::PARAM_STR );
  $rs->bindValue(':avatar', $_POST['avatar'], \PDO::PARAM_STR );
  $rs->execute();
  return $connexion -> lastInsertId();
}

function delete(\PDO $connexion, int $id) {
  $sql = "DELETE
          FROM authors
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT );
  return intval($rs->execute());
}

function update(\PDO $connexion, int $id) {
  $sql = "UPDATE authors
          SET firstname = :firstname,
              lastname = :lastname,
              biography = :biography,
              avatar = :avatar
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':firstname', $_POST['firstname'], \PDO::PARAM_STR );
  $rs->bindValue(':lastname', $_POST['lastname'], \PDO::PARAM_STR );
  $rs->bindValue(':biography', $_POST['biography'], \PDO::PARAM_STR );
  $rs->bindValue(':avatar', $_POST['avatar'], \PDO::PARAM_STR );
  $rs->bindValue(':id', $id, \PDO::PARAM_INT );
  return intval($rs->execute());

}
