<?php
/*
 ./app/vues/authors/index.php
 Variables disponibles:
   - $authors ARRAY(ARRAY(id, firstname,lastname,biography,avatar,created_at))
*/
?>
<div class="jumbotron">
 <h1>Gestion des authors</h1>
</div>
<div class="">
   <a href="authors/add/form">Ajouter un author</a>
</div>
<table class="table table-striped">
 <thead>
   <tr>
     <th>#</th>
     <th>FirstName</th>
     <th>LastName</th>
     <th>biography</th>
     <th>Created_at</th>
     <th>Avatar</th>
     <th>Actions</th>
   </tr>
 </thead>
 <tbody>
   <?php foreach ($authors as $author): ?>
     <tr>
       <td><?php echo $author['id']; ?></td>
       <td><?php echo $author['firstname']; ?></td>
       <td><?php echo $author['lastname']; ?></td>
       <td><?php echo $author['biography']; ?></td>
       <td><?php echo $author['created_at']; ?></td>
       <td><img src="assets/img/blog/<?php echo $author['avatar']; ?>" alt="image test"></td>
       <td>
         <a href="authors/edit/form/<?php echo $author['id']; ?>" class="edit">Edit</a> |
         <a href="authors/delete/<?php echo $author['id']; ?>" class="delete">Delete</a>
       </td>
     </tr>
   <?php endforeach; ?>
 </tbody>
</table>
