<?php
/*
 ./app/vues/categories/addForm.php

*/
?>
<h1>Ajout d'un enregistrement</h1>
<div>
  <a href="authors">
    Retour vers la liste des enregistrements
  </a>
</div>
<form action="authors/add/insert" method="post">
  <div class="form-group">
    <label for="firstname">firstname</label>
    <input type="text" id="firstname" name="firstname">
  </div>
  <div class="form-group">
    <label for="lastname">lastname</label>
    <input type="text" id="lastname" name="lastname">
  </div>
  <div class="form-group">
    <label for="biography">biography</label>
    <input type="text" id="biography" name="biography">
  </div>
  <div class="form-group">
    <label for="avatar">avatar</label>
    <input type="file" id="avatar" name="avatar">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
