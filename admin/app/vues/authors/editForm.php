<?php
/*
 ./app/vues/authors/editForm.php
 Variables disponibles:
- $authors ARRAY(ARRAY(id, firstname,lastname,biography,avatar,created_at))
*/
?>
<h1>Modification d'un enregistrement</h1>
<div>
  <a href="authors">
    Retour vers la liste des enregistrements
  </a>
</div>
<form action="authors/edit/<?php echo $author['id'] ?>" method="post" class="edit">
  <div class="form-group">
    <label for="firstname">firstname</label>
    <input type="text" id="firstname" name="firstname" value="<?php echo $author['firstname'] ?>">
  </div>
  <div class="form-group">
    <label for="lastname">lastname</label>
    <input type="text" id="lastname" name="lastname" value="<?php echo $author['lastname'] ?>">
  </div>
  <div class="form-group">
    <label for="biography">biography</label>
    <input type="text" id="biography" name="biography" value="<?php echo $author['biography'] ?>">
  </div>
  <div class="form-group">
    <label for="avatar">avatar</label>
    <input type="file" id="avatar" name="avatar">
  </div>
  <button type="submit" class="btn btn-primary">Modifier</button>
</form>
