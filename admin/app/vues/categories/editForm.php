<?php
/*
 ./app/vues/categories/editForm.php
 Variables disponibles:
   - $categories ARRAY(ARRAY(id, name, created_at))
*/
?>
<h1>Modification d'un enregistrement</h1>
<div>
  <a href="categories">
    Retour vers la liste des enregistrements
  </a>
</div>
<form action="categories/edit/<?php echo $categorie['id'] ?>" method="post" class="edit">
  <div class="form-group">
    <label for="titre">Titre</label>
    <input type="text" id="titre" name="titre" value="<?php echo $categorie['name'] ?>">
  </div>

  <!-- <div class="form-group">
    <label for="slug">Slug</label>
    <input type="text" id="slug" name="slug">
  </div> -->
  <button type="submit" class="btn btn-primary">Modifier</button>
</form>
