<?php
/*
 ./app/vues/categories/addForm.php

*/
?>
<h1>Ajout d'un enregistrement</h1>
<div>
  <a href="categories">
    Retour vers la liste des enregistrements
  </a>
</div>
<form action="categories/add/insert" method="post">
  <div class="form-group">
    <label for="titre">Titre</label>
    <input type="text" id="titre" name="titre">
  </div>
  <!-- <div class="form-group">
    <label for="slug">Slug</label>
    <input type="text" id="slug" name="slug">
  </div> -->
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
