<?php
/*
 ./app/routeurs/user.php
*/

include '../app/controleurs/authorsControleur.php';

switch ($_GET['authors']) {
  case 'addForm':
  // AJOUT D'UNE author : FORMULAIRE
  // PATTERN: index.php?authors=addForm
  // CTRL: authorsControleur
  // ACTION: addForm
    \App\Controleurs\authorsControleur\addFormAction();
    break;
  case 'add':
  // AJOUT D'UNE author : INSERT
  // PATTERN: index.php?authors=add
  // CTRL: authorsControleur
  // ACTION: add
    \App\Controleurs\authorsControleur\addAction($connexion);
    break;
  case 'delete':
  // SUPPRESSION D'UNE author : DELETE
  // PATTERN: index.php?authors=delete&id=x
  // CTRL: authorsControleur
  // ACTION: delete
    \App\Controleurs\authorsControleur\deleteAction($connexion, $_GET['id']);
    break;
  case 'editForm':
  // MODIFICATION D'UNE author : FORMULAIRE
  // PATTERN: index.php?authors=editForm&id=x
  // CTRL: authorsControleur
  // ACTION: editForm
    \App\Controleurs\authorsControleur\editFormAction($connexion, $_GET['id']);
    break;
  case 'edit':
  // MODIFICATION D'UNE author : UPDATE
  // PATTERN: index.php?authors=edit&id=x
  // CTRL: authorsControleur
  // ACTION: edit
    \App\Controleurs\authorsControleur\editAction($connexion, $_GET['id']);
    break;
  default:
  // LISTE DES authorS
  // PATTERN: index.php?authors=index
  // CTRL: catgeoriesControleur
  // ACTION: index
   \App\Controleurs\AuthorsControleur\indexAction($connexion);
   break;
}
