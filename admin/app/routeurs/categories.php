<?php
/*
 ./app/routeurs/user.php
*/

include '../app/controleurs/categoriesControleur.php';

switch ($_GET['categories']) {
  case 'addForm':
  // AJOUT D'UNE CATEGORIE : FORMULAIRE
  // PATTERN: index.php?categories=addForm
  // CTRL: categoriesControleur
  // ACTION: addForm
    \App\Controleurs\CategoriesControleur\addFormAction();
    break;
  case 'add':
  // AJOUT D'UNE CATEGORIE : INSERT
  // PATTERN: index.php?categories=add
  // CTRL: categoriesControleur
  // ACTION: add
    \App\Controleurs\CategoriesControleur\addAction($connexion);
    break;
  case 'delete':
  // SUPPRESSION D'UNE CATEGORIE : DELETE
  // PATTERN: index.php?categories=delete&id=x
  // CTRL: categoriesControleur
  // ACTION: delete
    \App\Controleurs\CategoriesControleur\deleteAction($connexion, $_GET['id']);
    break;
  case 'editForm':
  // MODIFICATION D'UNE CATEGORIE : FORMULAIRE
  // PATTERN: index.php?categories=editForm&id=x
  // CTRL: categoriesControleur
  // ACTION: editForm
    \App\Controleurs\CategoriesControleur\editFormAction($connexion, $_GET['id']);
    break;
  case 'edit':
  // MODIFICATION D'UNE CATEGORIE : UPDATE
  // PATTERN: index.php?categories=edit&id=x
  // CTRL: categoriesControleur
  // ACTION: edit
    \App\Controleurs\CategoriesControleur\editAction($connexion, $_GET['id']);
    break;
  default:
  // LISTE DES CATEGORIES
  // PATTERN: index.php?categories=index
  // CTRL: catgeoriesControleur
  // ACTION: index
   \App\Controleurs\CategoriesControleur\indexAction($connexion);
   break;
}
