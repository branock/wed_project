
$(function(){

  $('.edit').submit(function(){
    if(!confirm("Voulez vous vraiment modifier cet enregistrement ?")){
      return false;
    }
  });
  $('.delete').click(function(){
    if(!confirm("Voulez vous vraiment supprimer cet enregistrement ?")){
      return false;
    }
  });
});
