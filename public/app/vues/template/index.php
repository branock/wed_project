<?php
/*
  ../app/vues/template/index.php
  TEMPLATE PRINCIPAL
*/
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <?php include '../app/vues/template/partials/_head.php' ?>
  </head>

  <body>
    <?php include '../app/vues/template/partials/_preload.php' ?>

    <?php include '../app/vues/template/partials/_header.php' ?>

    <?php include '../app/vues/template/partials/_main.php' ?>

    <?php include '../app/vues/template/partials/_footer.php' ?>

    <?php include '../app/vues/template/partials/_scripts.php' ?>
  </body>
</html>
